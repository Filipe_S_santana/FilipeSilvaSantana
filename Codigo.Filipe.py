da importação openpyxl load_workbook

# Carregar o arquivo Excel
pasta de trabalho = load_workbook(filename='DATASET.xlsx')

# Obter a primeira planilha
folha = pasta de trabalho.active

# Criar uma lista para armazenar os dados
data = []

# Iterar pelas linhas da planilha a partir da linha 2
for row in sheet.iter_rows(min_row=2, values_only=True):
    nome = row[0]
    sobrenome = row[1]
    peso = row[2]
    altura = row[3]

    # Verificar se o valor da altura não é nulo
    if altura is not None:
        altura = float(altura)
    else:
        altura = 0.0

    # Verificar se o valor do peso não é nulo
    if peso is not None:
        peso = float(peso)
    else:
        peso = 0.0

    # Calcular o IMC apenas se a altura for maior que zero
    if altura > 0:
        imc = peso / (altura ** 2)
        imc = round(imc, 2)  # Arredondar o IMC para 2 casas decimais
    else:
        imc = 0.0

    # Adicionar os dados na lista
    data.append((nome + sobrenome, imc))

# Salvar as informações em um arquivo de texto
with open('resultado_imc.txt', 'w') as file:
    for item in data:
        nome_formatado = item[0].upper()  # Converter a primeira letra para maiúscula
        file.write("Nome: {}\n".format(nome_formatado))
        file.write("IMC: {:.2f}\n".format(item[1]))  # Formatar o IMC com duas casas decimais
        file.write("\n")

print("As informações foram salvas em 'resultado_imc.txt'.")
